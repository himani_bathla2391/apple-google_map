//
//  ViewController.h
//  apple map
//
//  Created by Clicklabs 104 on 10/14/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>


@end

