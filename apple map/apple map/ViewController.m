//
//  ViewController.m
//  apple map
//
//  Created by Clicklabs 104 on 10/14/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *list;
NSMutableArray *result;
@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *b;
@property (weak, nonatomic) IBOutlet UISearchBar *search;

@property (weak, nonatomic) IBOutlet UITableView *table;

@property (weak, nonatomic) IBOutlet MKMapView *applemap;

@property (weak, nonatomic) IBOutlet GMSMapView *googlemap;
@property (weak, nonatomic) IBOutlet UILabel *l1;

@property (weak, nonatomic) IBOutlet UILabel *l2;
@end

@implementation ViewController

@synthesize search;
@synthesize table;
@synthesize applemap;
@synthesize googlemap;
MKPointAnnotation *annotationPoint;
MKPointAnnotation *annotationPoint2 ;
MKPointAnnotation *annotationPoint3 ;
- (void)viewDidLoad {
    [super viewDidLoad];
    list=[[NSMutableArray alloc]initWithObjects:@"Chandigarh",@"Faridabad",@"Rohtak", nil];
    result = [NSMutableArray arrayWithArray:list];
    [table reloadData];
//    table.hidden= YES;
    
//    CLLocationCoordinate2D annotationCoord;
//    
//    annotationCoord.latitude = 30.7500;
//    annotationCoord.longitude = 76.7800;
    
  
    
    

   
    
    
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)searchBar:(UISearchBar *)search textDidChange:(NSString *)searchText {
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    result = [NSMutableArray arrayWithArray: [list filteredArrayUsingPredicate:resultPredicate]];
    [table reloadData];
    table.hidden=NO;
    
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [result count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView
                              dequeueReusableCellWithIdentifier:@"cellReuse"];
    
    cell.textLabel.text = result[indexPath.row];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    search.text = nil;
//    
    if (indexPath.row == 0)
    {
        [googlemap clear];
        [[applemap viewForAnnotation:annotationPoint2] setHidden:YES];
        [[applemap viewForAnnotation:annotationPoint3] setHidden:YES];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(30.7500, 76.7800);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = @"Chandigarh";
        marker.map = googlemap;
        marker.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
        
        annotationPoint = [[MKPointAnnotation alloc] init];
        annotationPoint.coordinate = position;
        annotationPoint.title = @"Chandigarh";
        //annotationPoint.subtitle = @"Chandigarh is here";
        [applemap addAnnotation:annotationPoint];
    }

    else if(indexPath.row == 1){
        [googlemap clear];
        [[applemap viewForAnnotation:annotationPoint] setHidden:YES];
        [[applemap viewForAnnotation:annotationPoint3] setHidden:YES];
        CLLocationCoordinate2D position2 = CLLocationCoordinate2DMake(28.4211, 77.3078);
        GMSMarker *marker2 = [GMSMarker markerWithPosition:position2];
        marker2.title = @"Faridabad";
        marker2.map = googlemap;
        marker2.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
        
      annotationPoint2 = [[MKPointAnnotation alloc] init];
        annotationPoint2.coordinate = position2;
        annotationPoint2.title = @"Faridabad";
        [applemap addAnnotation:annotationPoint2];
        
    }
    else if (indexPath.row == 2){
        [googlemap clear];
        [[applemap viewForAnnotation:annotationPoint] setHidden:YES];
        [[applemap viewForAnnotation:annotationPoint2] setHidden:YES];
        CLLocationCoordinate2D position3 = CLLocationCoordinate2DMake(28.8909, 76.5796);
        GMSMarker *marker3 = [GMSMarker markerWithPosition:position3];
        marker3.title = @"Rohtak";
        marker3.map = googlemap;
        marker3.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
        
        annotationPoint3 = [[MKPointAnnotation alloc] init];
        annotationPoint3.coordinate = position3;
        annotationPoint3.title = @"Rohtak";
        [applemap addAnnotation:annotationPoint3];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
